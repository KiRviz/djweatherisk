//
//  WeatherModelViewsTests.swift
//  djweatherisk
//
//  Created by Darius Jankauskas on 17/05/2017.
//  Copyright © 2017 Darius Jankauskas. All rights reserved.
//

import XCTest
@testable import djweatherisk

class WeatherViewModelsTests: XCTestCase {
    func testWeatherSummaryViewModelInstantiation() {
        let model = WeatherSummary(summary: "Bad weather", icon: .cloudy)
        let viewModel = WeatherSummaryViewModel(model: model)
        XCTAssertEqual(viewModel.summary, model.summary)
        XCTAssertNotNil(viewModel.picture)
        XCTAssertEqual(viewModel.picture?.hashValue, UIImage(named: "cloudy")?.hashValue)
    }

    func testWeatherAlertSummaryViewModelInstantiation() {
        let model = WeatherAlert(title: "Dangerous", description: "Very dangerous", time: Date(timeIntervalSince1970: 0), expires: Date(timeIntervalSince1970: 1453407300))
        let viewModel = WeatherAlertSummaryViewModel(model: model)
        XCTAssertEqual(viewModel.title, model.title)
        // clear indication that this is a buggy test (depends on current locale)
        XCTAssertEqual(viewModel.time, Date(timeIntervalSince1970: 0).mediumTime)
    }
    
    func testWeatherForecasViewModelInstantiation() {
        let currently = WeatherSummary(summary: "currently summary", icon: .cloudy)
        let minutely = WeatherSummary(summary: "minutely summary", icon: .cloudy)
        let hourly = WeatherSummary(summary: "hourly summary", icon: .cloudy)
        let daily = WeatherSummary(summary: "daily summary", icon: .cloudy)
        let alert = WeatherAlert(title: "warning", description: "some alert", time: Date(timeIntervalSince1970: 0), expires: Date(timeIntervalSince1970: 1453407300))
        let forecast = WeatherForecast(currently: currently, minutely: minutely, hourly: hourly, daily: daily, alerts: [alert])
        let forecastViewModel = WeatherForecastViewModel(model: forecast)
        
        guard forecastViewModel.entries.count == 5 else {
            XCTFail("Forecast entries should be 5")
            return
        }
        
        XCTAssertNotNil(forecastViewModel.entries[0] as? WeatherAlertSummaryViewModel)
        XCTAssertNotNil(forecastViewModel.entries[1] as? WeatherSummaryViewModel)
        XCTAssertNotNil(forecastViewModel.entries[2] as? WeatherSummaryViewModel)
        XCTAssertNotNil(forecastViewModel.entries[3] as? WeatherSummaryViewModel)
        XCTAssertNotNil(forecastViewModel.entries[4] as? WeatherSummaryViewModel)
    }
}

extension Date {
    var mediumTime: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .medium
        return dateFormatter.string(from: self)
    }
}
