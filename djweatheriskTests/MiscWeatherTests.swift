//
//  MiscWeatherTests.swift
//  MiscWeatherTests
//
//  Created by Darius Jankauskas on 17/05/2017.
//  Copyright © 2017 Darius Jankauskas. All rights reserved.
//

import XCTest
@testable import djweatherisk

class MiscWeatherTests: XCTestCase {
    func testAllWeatherImagesExist() {
        for icon in WeatherIcon.allValues {
            let model = WeatherSummary(summary: "Bad weather", icon: icon)
            let viewModel = WeatherSummaryViewModel(model: model)
            XCTAssertNotNil(viewModel.picture, "Image \(icon.iconFileName()) does not exist")
        }
    }
}
