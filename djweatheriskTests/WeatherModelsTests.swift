//
//  WeatherModelsTests.swift
//  djweatherisk
//
//  Created by Darius Jankauskas on 17/05/2017.
//  Copyright © 2017 Darius Jankauskas. All rights reserved.
//

import XCTest
@testable import djweatherisk
import SwiftyJSON

class WeatherModelsTests: XCTestCase {
    let newYorkJson = loadJson(name: "NewYorkWeatherExample")
    
    func testWeatherSummaryLoadFromJson() {
        guard let summary = WeatherSummary(json: newYorkJson["currently"]) else {
            XCTFail()
            return
        }
        
        XCTAssertEqual(summary.summary, "Rain")
        XCTAssertEqual(summary.icon, .rain)
    }
    
    func testWeatherAlertLoadFromJson() {
        
        guard let json = newYorkJson["alerts"].array?[0],
            let alert = WeatherAlert(json: json) else {
            XCTFail()
            return
        }
        
        XCTAssertEqual(alert.title, "Flood Watch for Mason, WA")
        XCTAssertEqual(alert.description, "...FLOOD WATCH REMAINS IN EFFECT THROUGH LATE FRIDAY NIGHT...\nTHE FLOOD WATCH CONTINUES FOR\n* A PORTION OF NORTHWEST WASHINGTON...INCLUDING THE FOLLOWING\nCOUNTY...MASON.\n* THROUGH LATE FRIDAY NIGHT\n* A STRONG WARM FRONT WILL BRING HEAVY RAIN TO THE OLYMPICS\nTONIGHT THROUGH THURSDAY NIGHT. THE HEAVY RAIN WILL PUSH THE\nSKOKOMISH RIVER ABOVE FLOOD STAGE TODAY...AND MAJOR FLOODING IS\nPOSSIBLE.\n* A FLOOD WARNING IS IN EFFECT FOR THE SKOKOMISH RIVER. THE FLOOD\nWATCH REMAINS IN EFFECT FOR MASON COUNTY FOR THE POSSIBILITY OF\nAREAL FLOODING ASSOCIATED WITH A MAJOR FLOOD.\n")
        XCTAssertEqual(alert.expires.iso8601, "2016-01-21T20:15:00.000Z")
        XCTAssertEqual(alert.expires.iso8601, "2016-01-21T20:15:00.000Z")
    }
    
    func testWeatherForecastLoadsSummariesFromJson() {
        guard let forecast = WeatherForecast(json: newYorkJson) else {
            XCTFail()
            return
        }
        
        XCTAssertEqual(forecast.currently.summary, "Rain")
        XCTAssertEqual(forecast.currently.icon, .rain)
        XCTAssertEqual(forecast.minutely.summary, "Rain for the hour.")
        XCTAssertEqual(forecast.minutely.icon, .rain)
        XCTAssertEqual(forecast.hourly.summary, "Rain throughout the day.")
        XCTAssertEqual(forecast.hourly.icon, .rain)
        XCTAssertEqual(forecast.daily.summary, "Light rain throughout the week, with temperatures bottoming out at 48°F on Sunday.")
        XCTAssertEqual(forecast.daily.icon, .rain)
    }

    func testWeatherForecastLoadsAlertsFromJson() {
        guard let forecast = WeatherForecast(json: newYorkJson) else {
            XCTFail()
            return
        }
        
        guard forecast.alerts.count == 1 else {
            XCTFail("alerts count should be 1")
            return
        }
        XCTAssertEqual(forecast.alerts[0].description, "...FLOOD WATCH REMAINS IN EFFECT THROUGH LATE FRIDAY NIGHT...\nTHE FLOOD WATCH CONTINUES FOR\n* A PORTION OF NORTHWEST WASHINGTON...INCLUDING THE FOLLOWING\nCOUNTY...MASON.\n* THROUGH LATE FRIDAY NIGHT\n* A STRONG WARM FRONT WILL BRING HEAVY RAIN TO THE OLYMPICS\nTONIGHT THROUGH THURSDAY NIGHT. THE HEAVY RAIN WILL PUSH THE\nSKOKOMISH RIVER ABOVE FLOOD STAGE TODAY...AND MAJOR FLOODING IS\nPOSSIBLE.\n* A FLOOD WARNING IS IN EFFECT FOR THE SKOKOMISH RIVER. THE FLOOD\nWATCH REMAINS IN EFFECT FOR MASON COUNTY FOR THE POSSIBILITY OF\nAREAL FLOODING ASSOCIATED WITH A MAJOR FLOOD.\n")
        XCTAssertEqual(forecast.alerts[0].expires.iso8601, "2016-01-21T20:15:00.000Z")
    }

    
}

func loadJson(name: String) -> JSON {
    let bundle = Bundle(for: WeatherModelsTests.self)
    let path = bundle.path(forResource: name, ofType: "json")!
    let url = URL(fileURLWithPath: path)
    let data = try! Data(contentsOf: url, options: .mappedIfSafe)
    return try! JSON(data: data)
}

extension Formatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        return formatter
    }()
}
extension Date {
    var iso8601: String {
        return Formatter.iso8601.string(from: self)
    }
}
extension String {
    var iso8601: Date? {
        return Formatter.iso8601.date(from: self)
    }
}
