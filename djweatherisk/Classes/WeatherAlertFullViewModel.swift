//
//  WeatherAlertFullViewModel.swift
//  djweatherisk
//
//  Created by Darius Jankauskas on 18/05/2017.
//  Copyright © 2017 Darius Jankauskas. All rights reserved.
//

import Foundation

struct WeatherAlertFullViewModel {
    let title: String
    let description: String
    let time: String
    let expires: String
}

// MARK: Instantiate from model
extension WeatherAlertFullViewModel {
    init(model: WeatherAlert) {
        title = model.title
        description = model.description
        time = WeatherAlertSummaryViewModel.formattedForMediumTime(from: model.time)
        let formattedExpiration = WeatherAlertFullViewModel.formattedForMediumDateAndeTime(from: model.expires)
        expires = NSLocalizedString("Expires\n\(formattedExpiration)", comment: "")
    }
    
    static func formattedForMediumDateAndeTime(from date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .medium
        return dateFormatter.string(from: date)
    }
}
