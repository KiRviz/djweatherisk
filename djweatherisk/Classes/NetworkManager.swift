//
//  NetworkManager.swift
//  djweatherisk
//
//  Created by Darius Jankauskas on 18/05/2017.
//  Copyright © 2017 Darius Jankauskas. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class NetworkManager {
    let apiKey = "cb7a09d475485cc42c6bf8bd93099d4c"
    
    let baseUrl = "https://api.darksky.net/forecast"
    
    let londonCoordinates = "51.5074,-0.1278"
    let units = "si"
    let language = "en"
    let exclude = "flags"
    
    func getWeatherForecast(completion: @escaping (JSON?, Error?)->()) {
        // // uncomment to respond with fake New York Data (in order to see how alerts look)
        fakeWeatherForecast(completion: completion)
        return
        
        
        let fullUrl = "\(baseUrl)/\(apiKey)/\(londonCoordinates)"
        let parameters: Parameters = ["units": units, "language": language]
        
        Alamofire.request(fullUrl, parameters: parameters).validate().responseJSON { (responce) in
            switch (responce.result) {
            case .success(let value):
                completion(JSON(value), nil)
            case .failure(let error):
                print("Error getting wather forecast: \(error)")
                completion(nil, error)
            }
        }
    }
}

// MARK: SampleData
// TODO: Would be nice to write a UI test with a stub
// TODO: As for inspecting in the live app, could use some in-app debug-only menu to trigger this
extension NetworkManager {
    func fakeWeatherForecast(completion: @escaping (JSON?, Error?)->()) {
        let json = loadJson(name: "NewYorkWeatherExample")
        completion(json, nil)
    }
    
    func loadJson(name: String) -> JSON {
        let path = Bundle.main.path(forResource: name, ofType: "json")!
        let url = URL(fileURLWithPath: path)
        let data = try! Data(contentsOf: url, options: .mappedIfSafe)
        return try! JSON(data: data)
    }
}
