//
//  WeatherForecast.swift
//  djweatherisk
//
//  Created by Darius Jankauskas on 17/05/2017.
//  Copyright © 2017 Darius Jankauskas. All rights reserved.
//

import Foundation
import SwiftyJSON

struct WeatherForecast {
    let currently: WeatherSummary
    let minutely: WeatherSummary
    let hourly: WeatherSummary
    let daily: WeatherSummary
    let alerts: [WeatherAlert]
}

// MARK: JSON
extension WeatherForecast {
    init?(json: JSON) {
        guard let currently = WeatherSummary(json: json["currently"]),
            let minutely =  WeatherSummary(json: json["minutely"]),
            let hourly =  WeatherSummary(json: json["hourly"]),
            let daily =  WeatherSummary(json: json["daily"]) else {
                return nil
        }
        
        let alerts: [WeatherAlert] = json["alerts"].array?.compactMap({ WeatherAlert(json: $0) }) ?? []
        
        
        self.init(currently: currently, minutely: minutely, hourly: hourly, daily: daily, alerts: alerts)
    }
}
