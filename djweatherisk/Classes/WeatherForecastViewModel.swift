//
//  WeatherForecastViewModel.swift
//  djweatherisk
//
//  Created by Darius Jankauskas on 17/05/2017.
//  Copyright © 2017 Darius Jankauskas. All rights reserved.
//

import UIKit

protocol WeatherCellPresentable {
    func cellInstance(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell
    func performSegue(controller: UIViewController)
    func prepare(for segue: UIStoryboardSegue)
}

struct WeatherForecastViewModel {
    let entries: [WeatherCellPresentable]
}

// MARK: Instantiate from model
extension WeatherForecastViewModel {
    init(model: WeatherForecast) {
        let alerts: [WeatherCellPresentable] = model.alerts.map { a in WeatherAlertSummaryViewModel(model: a) }
        let summaries: [WeatherCellPresentable] = [WeatherSummaryViewModel(model: model.currently),
                         WeatherSummaryViewModel(model: model.minutely),
                         WeatherSummaryViewModel(model: model.hourly),
                         WeatherSummaryViewModel(model: model.daily)]
        entries = alerts + summaries
    }
}
