//
//  WeatherAlert.swift
//  djweatherisk
//
//  Created by Darius Jankauskas on 18/05/2017.
//  Copyright © 2017 Darius Jankauskas. All rights reserved.
//

import Foundation
import SwiftyJSON

struct WeatherAlert {
    let title: String
    let description: String
    let time: Date
    let expires: Date
}

// MARK: JSON
extension WeatherAlert {
    init?(json: JSON) {
        guard let title = json["title"].string,
            let description = json["description"].string,
            let timeInt = json["time"].double,
            let expiresInt = json["expires"].double else {
                return nil
        }
        
        let time = Date(timeIntervalSince1970: TimeInterval(timeInt))
        let expires = Date(timeIntervalSince1970: TimeInterval(expiresInt))
        
        self.init(title: title, description: description, time: time, expires: expires)
    }
}
