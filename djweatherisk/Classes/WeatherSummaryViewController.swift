//
//  WeatherSummaryViewController.swift
//  djweatherisk
//
//  Created by Darius Jankauskas on 17/05/2017.
//  Copyright © 2017 Darius Jankauskas. All rights reserved.
//

import UIKit
import SwiftyJSON

class WeatherSummaryViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let network = NetworkManager()
    
    var forecastViewModel: WeatherForecastViewModel?
    var entries: [WeatherCellPresentable]? {
        return forecastViewModel?.entries
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        
        activityIndicator.startAnimating()
        network.getWeatherForecast(completion: handleGetForecast)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let entry = sender as AnyObject as? WeatherCellPresentable else {
            return
        }

        entry.prepare(for: segue)
    }
}

// MARK: Network
extension WeatherSummaryViewController {
    func handleGetForecast(json: JSON?, error: Error?) {
        guard error == nil else {
            showAlertAndRetry(message: NSLocalizedString("Something's wrong with our server or your connection", comment: ""))
            return
        }
        
        guard let unwrappedJson = json,
            let forecast = WeatherForecast(json: unwrappedJson) else {
                showAlertAndRetry(message: NSLocalizedString("Something went wrong, please let us know!", comment: ""))
                return
        }
        
        
        activityIndicator.stopAnimating()
        forecastViewModel = WeatherForecastViewModel(model: forecast)
        tableView.reloadData()
    }
    
    func showAlertAndRetry(message: String) {
        let alert = UIAlertController(title: NSLocalizedString("Oops", comment: ""),
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { [weak self] _ in
            self?.network.getWeatherForecast(completion: self!.handleGetForecast)
        })
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: Table View Data Source
extension WeatherSummaryViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return entries?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return entries![indexPath.row].cellInstance(tableView: tableView, indexPath: indexPath)
    }
}

extension WeatherSummaryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        entries![indexPath.row].performSegue(controller: self)
    }
}
