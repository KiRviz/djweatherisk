//
//  WeatherAlertViewController.swift
//  djweatherisk
//
//  Created by Darius Jankauskas on 18/05/2017.
//  Copyright © 2017 Darius Jankauskas. All rights reserved.
//

import UIKit

class WeatherAlertViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var expiresLabel: UILabel!
    
    var modelView: WeatherAlertFullViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
    }

    func setupView() {
        guard let modelView = modelView else {
            return
        }
        
        titleLabel.text = modelView.title
        descriptionLabel.text = modelView.description
        timeLabel.text = modelView.time
        expiresLabel.text = modelView.expires
    }
}
