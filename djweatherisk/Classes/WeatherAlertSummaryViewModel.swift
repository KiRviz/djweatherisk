//
//  WeatherAlertSummaryViewModel.swift
//  djweatherisk
//
//  Created by Darius Jankauskas on 18/05/2017.
//  Copyright © 2017 Darius Jankauskas. All rights reserved.
//

import UIKit

struct WeatherAlertSummaryViewModel {
    let model: WeatherAlert

    let title: String
    let time: String
}

// MARK: Instantiate from model
extension WeatherAlertSummaryViewModel {
    init(model: WeatherAlert) {
        self.model = model
        
        title = model.title
        time = WeatherAlertSummaryViewModel.formattedForMediumTime(from: model.time)
    }
    
    static func formattedForMediumTime(from date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .medium
        return dateFormatter.string(from: date)
    }
}

// MARK: WeatherCellPresentable
extension WeatherAlertSummaryViewModel: WeatherCellPresentable {
    var segueName: String {
        return "AlertSegue"
    }
    
    func performSegue(controller: UIViewController) {
        controller.performSegue(withIdentifier: segueName, sender: self)
    }
    
    func prepare(for segue: UIStoryboardSegue) {
        guard let controller = segue.destination as? WeatherAlertViewController else {
            return
        }
        
        controller.modelView = WeatherAlertFullViewModel(model: model)
    }
    
    func cellInstance(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlertCell", for: indexPath) as! WeatherAlertTableViewCell
        
        cell.setup(self)
        
        return cell
    }
}
