    //
//  WeatherSummaryViewModel.swift
//  djweatherisk
//
//  Created by Darius Jankauskas on 17/05/2017.
//  Copyright © 2017 Darius Jankauskas. All rights reserved.
//

import UIKit

struct WeatherSummaryViewModel {
    let summary: String
    let picture: UIImage?
}

// MARK: Instantiate from model
extension WeatherSummaryViewModel {
    init(model: WeatherSummary) {
        summary = model.summary
        picture = UIImage(named: model.icon.iconFileName())
    }
}

// MARK: WeatherCellPresentable
extension WeatherSummaryViewModel: WeatherCellPresentable {
    var segueName: String {
        return "SummarySegue"
    }
    
    func performSegue(controller: UIViewController) {
        controller.performSegue(withIdentifier: segueName, sender: self)
    }
    
    func prepare(for segue: UIStoryboardSegue) {

    }

    func cellInstance(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SummaryCell", for: indexPath) as! WeatherSummaryTableViewCell
        
        cell.setup(self)
        
        return cell
    }
}
