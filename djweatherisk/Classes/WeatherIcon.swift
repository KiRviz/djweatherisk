//
//  WeatherIcon.swift
//  djweatherisk
//
//  Created by Darius Jankauskas on 17/05/2017.
//  Copyright © 2017 Darius Jankauskas. All rights reserved.
//

import Foundation

enum WeatherIcon : String {
    case clearDay = "clear-day"
    case clearNight = "clear-night"
    case rain
    case snow
    case sleet
    case wind
    case fog
    case cloudy
    case partlyCloudyDay = "partly-cloudy-day"
    case partlyCloudyNight = "partly-cloudy-night"
    case undefined
    
    // allValues are only used for testing but it must be defined here so that it's guaranteed to be updated
    // The alternative could be using undocumented behaviour to loop through all values of enum http://stackoverflow.com/a/28341290/603197
    // Another alternative could be to use Int enum and have an extra value called count
    static let allValues = [clearDay, clearNight, rain, snow, sleet, wind, fog, cloudy, partlyCloudyDay, partlyCloudyNight, undefined]
    
    init(rawValueOrDefault rawValue: String) {
        guard let icon = WeatherIcon(rawValue: rawValue) else {
            self = .undefined
            return
        }
        
        self = icon
    }
}

// MARK: Icon extension
extension WeatherIcon {
    func iconFileName() -> String {
        // Was tempting to reuse the rawValues but that would break the images whenever API mapping changes
        switch self {
        case .clearDay: return "clearDay"
        case .clearNight: return "clearNight"
        case .cloudy: return "cloudy"
        case .fog: return "fog"
        case .partlyCloudyDay: return "partlyCloudyDay"
        case .partlyCloudyNight: return "partlyCloudyNight"
        case .rain: return "rain"
        case .sleet: return "sleet"
        case .snow: return "snow"
        case .undefined: return "undefined"
        case .wind: return "wind"
        }
    }
}
