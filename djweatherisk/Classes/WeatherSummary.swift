//
//  WeatherSummary.swift
//  djweatherisk
//
//  Created by Darius Jankauskas on 17/05/2017.
//  Copyright © 2017 Darius Jankauskas. All rights reserved.
//

import Foundation
import SwiftyJSON

struct WeatherSummary {
    let summary: String
    let icon: WeatherIcon
}

// MARK: JSON
extension WeatherSummary {
    init?(json: JSON) {
        guard let summary = json["summary"].string,
            let iconString = json["icon"].string else {
                return nil
        }
            
        self.init(summary: summary, icon: WeatherIcon(rawValueOrDefault: iconString))
    }
}
