# Read me 

This is a little TDD experiment app

The detailview is only implemented for a red weaather alert and not for a normal weather entry.

Since there are no alerts in London I left a debug JSON in this revision (this debug JSON also shows degrees Farenheit, etc.). Delete lines 25 & 26 in NetworkManager to see the live API data, there's also a single compiler warning conveniently pointing to just below those lines.

But really not much point running the app, the point here is TDD.
